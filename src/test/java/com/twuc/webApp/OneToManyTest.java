package com.twuc.webApp;

import com.twuc.webApp.domain.Office;
import com.twuc.webApp.domain.OfficeRepository;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyTest extends JpaTestBase {
    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void create_table() {
    }

    @Test
    void should_save_office_and_staff() {
        Staff staff = new Staff("hua");
        Office office = new Office("Xi'an");
        office.getStaffs().add(staff);

        clear(em -> {
            officeRepository.save(office);
            em.flush();
        });

        Office officeFound = officeRepository.findById(1L).get();
        assertEquals(0, officeFound.getStaffs().size());// 没存关系
        assertEquals(1, office.getStaffs().size());
        assertNotSame(office, officeFound);
        assertEquals(1, staffRepository.findAll().size());// staff cascade 存进去
    }

    @Test
    void should_delete_office_and_staff() {
        Staff staff = new Staff("hua");
        Office office = new Office("Xi'an");
        office.getStaffs().add(staff);
        staff.setOffice(office);

        clear(em -> {
            officeRepository.save(office);
            em.flush();
        });

        Office officeFound = officeRepository.findById(1L).get();
        assertEquals(1, officeFound.getStaffs().size());
        assertEquals(1, staffRepository.findAll().size());

        officeRepository.delete(officeFound);
        officeRepository.flush();

        assertEquals(0, staffRepository.findAll().size());
    }
}
