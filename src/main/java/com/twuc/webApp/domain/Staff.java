package com.twuc.webApp.domain;

//import org.springframework.data.annotation.Id;

import javax.persistence.*;

@Entity
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 32)
    private String name;

    @ManyToOne
    private Office office;

    public Staff() {
    }

    public Staff(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }
}
